const express = require('express');
const sequelize = require('./database/sequelize');

sequelize.sync({ alter: true });

const app = express();

app.get('/', (req, res) => {
    res.status(200).json({ message: 'Hello world!' });
})

const port = 3000;
app.listen(port, () => {
  console.log(`Server running at ${port}`);
});
